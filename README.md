# TurtleBot3

This repository is a clone of the turtlebot simulation repo: https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git

We took the "ros2" branch of that repository and pushed it in this reporsitory.

We have made modification to the launch files so that gazebo is run in server mode. This makes it ideal for running the simulations in docker. Also, we have enabled the camera capabilities of waffle_pi robot in simulation.